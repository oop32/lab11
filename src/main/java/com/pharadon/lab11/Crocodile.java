package com.pharadon.lab11;

public class Crocodile extends Animal implements Crawlable ,Swimable{

    public Crocodile(String name, int numOfLeg) {
        super(name, 4);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void swim() {
        System.out.println(toString()+" swim.");
        
    }

    @Override
    public void crawl() {
        System.out.println(toString()+" crawl.");
        
    }

    @Override
    public void eat() {
        System.out.println(toString()+" eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(toString()+" sleep.");
        
    }

    @Override
    public String toString() {
        return ("Crocodile "+ this.getName());
    }
    
}
