package com.pharadon.lab11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat = new Bat("bat1");
        bat.eat();
        bat.sleep();
        bat.takeoff();
        bat.fly();
        bat.landing();

        System.out.println("");

        Fish fish1 = new Fish("Pra");
        fish1.eat();
        fish1.sleep();

        System.out.println("");

        Plane plane1 = new Plane("boeing121", "V8");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();
        
        System.out.println("");

        Crocodile crocodile1 = new Crocodile("Thong", 4);
        crocodile1.eat();
        crocodile1.crawl();
        crocodile1.sleep();

        System.out.println("");

        Submarine submarine1 = new Submarine("Subof10", "V100");
        submarine1.swim();

        System.out.println("");

        Bird bird = new Bird("Nok1", 2);
        bird.eat();
        bird.takeoff();
        bird.landing();
        bird.sleep();

        System.out.println("");

        Flyable[] flyableobjects = {bat,plane1,bird};
        for (int i = 0 ; i<flyableobjects.length;i++) {
            flyableobjects[i].takeoff();
            flyableobjects[i].fly();
            flyableobjects[i].landing();
        }

        Swimable[] swimablesobjects = {fish1,submarine1,crocodile1}; //ทดสอบการทำงาน
        for (int i = 0 ; i< swimablesobjects.length;i ++){
            swimablesobjects[i].swim();
        }
        
    }
}
