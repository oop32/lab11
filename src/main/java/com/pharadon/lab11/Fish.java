package com.pharadon.lab11;

public class Fish extends Animal implements Swimable{
    public Fish(String name){
        super(name, 0);
    }


    @Override
    public void eat() {
        System.out.println(toString()+" eat.");
    }
    @Override
    public void sleep() {
        System.out.println(toString()+" sleep.");
    }

    @Override
    public String toString() {
        return "Fish("+this.getName()+")";
    }

    public void swim() {
        System.out.println(toString()+" swim.");
    }
}