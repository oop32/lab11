package com.pharadon.lab11;

import javax.swing.UIDefaults.ProxyLazyValue;

public abstract class Vehicle {
    protected String name;
    private String engineName;

    public Vehicle(String name,String engineName){
        this.name = name;
        this.engineName = engineName;
    }

    public String getName() {
        return name;
    }

    public String getEngineName() {
        return engineName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnginename(String engineName){
        this.engineName = engineName;
    }
}
