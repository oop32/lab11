package com.pharadon.lab11;

public class Bird extends Animal implements Flyable{

    public Bird(String name, int numOfLeg) {
        super(name, 2);
        
    }

    @Override
    public void fly() {
        System.out.println(this.toString()+" fly.");
        
    }

    @Override
    public void landing() {
        System.out.println(this.toString()+" landing.");
        
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString()+" takeoff.");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+" eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(toString()+" sleep");
        
    }

    @Override
    public String toString() {
        return ("Bird "+this.getName());
    }
    
}
