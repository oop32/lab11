package com.pharadon.lab11;

public abstract class Animal {
    private String name ;
    private int numOfLeg ;

    public Animal(String name,int numOfLeg){
        this.name = name;
        this.numOfLeg = numOfLeg;
    }

    public String getName(){
        return name;
    }

    public int getnumOfLeg(){
        return numOfLeg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setnumOfleg(int numOfLeg){
        this.numOfLeg =  numOfLeg;
    }

    @Override
    public String toString() {
        return "Animal ("+ name + ") " + numOfLeg +"leg " ;
    }

    public abstract void eat();
    public abstract void sleep();
}
